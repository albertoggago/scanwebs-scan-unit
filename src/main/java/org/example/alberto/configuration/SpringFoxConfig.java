package org.example.alberto.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;


@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Value("${apiInfo.title}")
    String title;

    @Value("${apiInfo.version}")
    String version;

    @Value("${apiInfo.description}")
    String description;

    @Value("${apiInfo.termsOfUse}")
    String termsOfService;

    @Value("${apiInfo.contact.name}")
    String contactName;

    @Value("${apiInfo.contact.url}")
    String contactUrl;

    @Value("${apiInfo.contact.email}")
    String contactEmail;

    @Value("${apiInfo.license}")
    String licence;

    @Value("${apiInfo.licenseUrl}")
    String licenceUrl;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                title,
                description,
                version,
                termsOfService,
                new Contact(contactName, contactUrl, contactEmail),
                licence,
                licenceUrl,
                Collections.emptyList());
    }

}

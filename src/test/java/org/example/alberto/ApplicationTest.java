package org.example.alberto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
class ApplicationTest {


    @Test
    void applicationStartTest() {

        Application.main(new String[] {});
        assertTrue(true);
    }

}
